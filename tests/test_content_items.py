from src.data import content_items


class TestContentItems:
    def test_content_items(self) -> None:
        assert isinstance(content_items, dict)
