.PHONY: compile render check fix lint fixlint format mypy deadcode audit test

render: .venv
	.venv/bin/python src/render.py


check: test lint mypy audit deadcode
fix: format fixlint

.venv:
	pip install uv
	uv venv -p 3.11
	uv pip sync requirements-dev.txt
	uv pip install -e .[test]


lint: .venv
	.venv/bin/ruff check src tests

fixlint: .venv
	.venv/bin/ruff check --fix src tests --unsafe-fixes
	.venv/bin/deadcode --fix src tests

format: .venv
	.venv/bin/ruff format src tests

mypy: .venv
	.venv/bin/mypy src tests

audit: .venv
	.venv/bin/pip-audit

deadcode: .venv
	.venv/bin/deadcode src tests

test: .venv
	.venv/bin/pytest $(PYTEST_ME_PLEASE)

sync: .venv
	uv pip sync requirements-dev.txt
	uv pip install -e .[test]

compile:
	uv pip compile -U -q pyproject.toml -o requirements.txt
	uv pip compile -U -q --all-extras pyproject.toml -o requirements-dev.txt
