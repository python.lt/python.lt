# python.lt

## Pakeitimų darymas
python.lt puslapyje yra rodomos šios repozitorijos `public` direktorijos turinys.
Norint padaryti pakeitimų python.lt svetainėje užtenka sukurti Pull Requestą, kuris
redaguoja `src/data.py` turinį.
Patvirtinus ir apjungus Pull Requestą į `main` šaką bus įvykdytas CI/CD pipeline,
kuris sukonstruos ir įdiegs `public` turinį į python.lt svetainę.

## Vizija
Aidžio brainstormo mintys:
- Nuorodos į konferencijas ir meetupus.
- Nuoroda į Python Lietuva facebook grupę
- Nuoroda į Angis
- Nuorodos į mokymosi šaltinius, prioritetas turėtų būti skiriamas lietuviškiems arba lietuvių kurtiems. Taip pat aukštos kokybės atrinkti šaltiniais anglų kalba.
- Nuoroda į twitter hashtagus ir userius
- Nuorodos į youtube kūrėjus. Pvz Griaustinis Tech
- Lietuvos kompanijų kuriose naudojamas Python aprašymas, įskaitant tai, kaip jose naudojamas Python.
- Studijų programos, mokymai. Dėl šito dvejoju, nes čia for profit dalykas. Bet gal su atitinkamu kritišku aprašymu ir perspėjimais aklai nepasitikėti, gal ir paeitų.

Apibendrinant įsivaizduoju kad python.lt turėtų būti trys ramsčiai
- Python
- Lietuvoje (Lietuviškos kompanijos, renginiai ir t.t.)
- Lietuviškai (Bandom palaikyti lietuvių kalbą)

Bei papildant labai aukštos kokybės informacija angliškai.


## Kaip prisidėti
- `python.lt` tinklapis yra sudarytas iš Python failų ir Jinja2 šablonų (stiliams naudojamas tailwindcss.com).
Norint sugeneruoti statinį `public/index.html` puslapį užtenka parašyti `make` šios repozitorijos šakniniame kataloge.
Ši komanda sukurs virtualią aplinką, parsiųs Python priklausomybes ir pergeneruos statinį `public/index.html` failą,
kuris yra rodomas `python.lt` tinklapyje.

Norėdami, kad jūsų padaryti pakeitimai atsirastų `python.lt` svetainėje:
- Pasidarykite `gitlab.com/python.lt/python.lt` repozitorijos kopiją su `Fork` funkcija.
- Padarykite kodo pakeitimus ir juos kartu su sugeneruotu puslapiu išsaugokite `Git` versijavimo sistemoje.
- Sukurkite pakeitimų apjungimo pasiūlymą (Pull request) į `gitlab.com/python.lt/python.lt`.

### Repozitorijos turinys
- `src` - Python kodas ir šablonai iš kurių generuojamas statinis `python.lt` puslapis.
- `public` - sugeneruoto `python.lt` tinklapio išeities failai.
- `tests` - Python unitų testai, kurie yra įvykdomi kaip `make check` komandos dalis.

### Naudingos komandos
Šis paketas naudoja `Makefile` dažniausiai naudojamų komandų trumpiniams. Komandinėje eilutėje įvykdžius:
- `make check` - bus paleidžiami unitų testai ir kodo linteriai.
- `make fix` - kodas bus automatiškai suformatuotas ir bus pataisytos pataisomos linterių klaidos.
- `make render` - bus iš naujo sudaromas statinis `python.lt` puslaplis faile `public/index.html`.
- `make compile` - bus pakeliamos ir užfiksuojamos priklausomybių versijos `requirements*.txt` failuose.
- `make sync` - bus atnaujinamos priklausomybių versijos virtualioje aplinkoje, kaip nurodyta `requirements*.txt` failuose (vykdoma po `make compile`)

### Priklausomybės
Šis Python projektas naudoja naujausius įrankius užtikrinti Python kodo kokybei:
- ruff - lintinimui ir kodo formatavimui.
- mypy - tipų tikrinimui.
- pip-audit - viešai žinomų pažeidžiamumų paieškai priklausomybėse.
- deadcode - nenaudojamo kodo aptikimui.
- pytest - unitų testų surinkimui ir įvykdymui.
- coverage - kodo padengimo testais nustatymui.
- uv - Python virtualios aplinkos ir priklausomybių valdymui (`pip`, `pip-tools` ir `virtualenv` atitikmuo).
- pyproject.toml - standartizuotas failas projekto ir priklausomybių konfigūracijai saugoti.
- Makefile - Unix sistemų įrankis leidžiantis apibrėžti komandinės eilutės komandų trumpinius.
