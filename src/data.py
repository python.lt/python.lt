"""Define content items which will be rendered into templates to generate static python.lt website.

Keys in content_items denote top level menu items.
List items denote information which will be rendered inside elements of flexbox page area.

Allowed item keys:
- title
- text (can contain HTML elements)
- link
- image
- video
- date (preferably used for news)
- section - news items are grouped into sections
- type - one of ("large", "small"), default - "small". "small" - takes 1/3 page and "large" - takes whole page width.
"""

content_items = {
    'Naujienos': [
        {
            'title': 'Mokytojai mokosi mokyti Python',
            'text': 'Informatikos mokytojai dalyvavo seminaruose apie Python programavimo kalbos mokymo patirtį',
            'link': 'https://if.ktu.edu/news/informatikos-mokytojai-dalyvavo-seminaruose-apie-pitono-programavimo-kalbos-mokymo-patirti/',
            'date': ' 2024-04-11',
        },
        {
            'title': 'Laikyk IT egzaminą su Python',
            'text': 'NSA informacinių technologijų egzamine nauja programavimo kalba',
            'link': 'https://www.nsa.smm.lt/2022/06/13/informaciniu-technologiju-egzamine-nauja-programavimo-kalba/',
            'date': ' 2022-06-13',
        },
    ],
    'Renginiai': [
        {
            'title': 'PyConLT',
            'text': 'PyCon Lithuania - kasmetinė tarptautinė konferencija',
            'link': 'https://pycon.lt/',
        },
        {
            'title': 'VilniusPy',
            'text': 'VilniusPy - Python meetupas Vilniuje',
            'link': 'https://www.meetup.com/VilniusPy',
        },
        {'title': 'KaunasPy', 'text': 'KaunasPy', 'link': 'https://www.meetup.com/KaunasPy'},
        {
            'title': 'PyData Vilnius',
            'text': 'PyData Vilnius - duomenų inžinerijos ir mašininio mokymosi meetupas Kaune',
            'link': 'https://www.meetup.com/PyData-Vilnius/',
        },
        {
            'title': 'PyData Kaunas',
            'text': 'PyData Kaunas - duomenų inžinerijos ir mašininio mokymosi meetupas Kaune',
            'link': 'https://www.meetup.com/PyData-Kaunas/',
        },
    ],
    'Bendruomenė': [
        {
            'title': 'Python Lietuva Facebook grupė',
            'text': 'Lietuvos Python bendruomenė telkiasi Facebook grupėje.',
            'link': 'https://www.facebook.com/groups/pythonlt/',
        },
        {
            'title': 'Lietuvių Python projektai',
            'text': 'Github projektai su žyme Lietuva',
            'link': 'https://github.com/topics/lithuanian',
        },
    ],
    'Išmok': [
        {
            'title': 'Angis',
            'text': 'Angis - lietuviškas įrankis mokytis Python',
            'link': 'https://angis.net/#/',
            'image': 'angis-black-logo.png',
            'section': 'pradinis',
        },
        {
            'title': 'Angies išeities kodas',
            'text': 'Angies išeities kodas pateikiamas github.com platformoje.',
            'link': 'https://github.com/mantasurbonas/angis',
            'section': 'pradinis',
        },
        {
            'title': 'Oficialus Python vadovas',
            'text': 'Oficialus Python pradžiamokslio vadovas anglų kalba',
            'link': 'https://docs.python.org/3/tutorial/index.html',
            'image': 'python_logo.png',
            'section': 'pradinis',
        },
        {
            'title': 'IT egzaminai su Python',
            'text': 'IT Brandos egzaminų sprendimai Python kalba',
            'link': 'https://github.com/python-dirbtuves/it-brandos-egzaminai',
            'section': 'vidutinis',
        },
        {
            'title': 'Python pamokos',
            'text': 'Programavimas Python (Youtube grojaraštis)',
            'link': 'https://www.youtube.com/watch?v=IuByH_vrwGA&list=PLB7EF2523A58A7854',
            'section': 'vidutinis',
        },
        {
            'title': 'Djano pamokos',
            'text': 'Griaustinis tech Django (Youtube grojaraštis)',
            'link': 'https://www.youtube.com/watch?v=998PannJtHo&list=PL3aaklOBGuJmxk9mrsbbWd45WYFyOWfLc&ab_channel=GriaustinisTech',
            'section': 'vidutinis',
        },
        {
            'title': 'LeetCode',
            'text': (
                'LeetCode - programavimo uždavinių platforma. Joje galima praktiškai išmokti duomenų struktūrų ir algoritmų. '
                'Šio platformoje taip pat galima pasipraktikuoti interviu iš IT gigantų kaip Google, Facebook, AWS.'
            ),
            'link': 'leetcode.com',
            'section': 'pažengęs',
        },
        {
            'title': 'HackerRank',
            'text': 'HackerRank - programavimo uždavinių platforma, kurioje galima išmokti duomenų struktūrų ir algoritmų.',
            'link': 'https://hackerrank.com',
            'section': 'pažengęs',
        },
        {
            'title': 'ProjectEuler',
            'text': 'ProjectEuler - nemokama Python uždavinių platforma orientuota į efektyvaus Python kodo rašymo išmokimą.',
            'link': 'https://projecteuler.net',
            'section': 'pažengęs',
        },
    ],
    'Taikymai': [
        {
            'title': 'Sukurk savo Python paketą',
            'text': 'Python ekosistema yra labai plati, galima įsirašyti viešai prieinamus paketus su pip install paketo pavadinimas komanda. Yra labai paprasta sukurti savo Python paketą, kuris taps prieinamas viesiems.',
            'link': 'https://github.com/albertas/modernpackage',
            'section': 'packages',
        },
        {
            'title': 'Pyodide',
            'text': 'Pyodide suteikia galimybę naršyklėje panaudoti praktiškai visas Python3 galimybes. Klaidų atveju yra pateikiamas aiškus Python Traceback. Pagrindinė problema yra užkrovimo laikas, kuris gali siekti kelias sekundes.',
            'link': 'https://pyodide.org/en/stable/',
            'section': 'Python naršyklėje',
        },
        {
            'title': 'PyScript',
            'text': 'Python gali būti naudojamas naršyklėje vietoje Javascripto. PyScript suteikia papildomus HTML elementus, kurie leidžia naudoti Python naršyklėje. Kalbos vykdymui yra panaudojamas Pyodide arba MicroPython (limituoto, bet greito Python) realizacijos.',
            'link': 'https://github.com/Semantika2/Lietuviu-kalbos-rasybos-tikrintuvai-bei-Hunspell-zodynai-gramatika',
            'section': 'Python naršyklėje',
        },
        {
            'title': 'Rašybos tikrinimas',
            'text': 'Lietuvių kalbos rašybos tikrintuvai bei Hunspell žodynai gramatika',
            'link': 'https://github.com/Semantika2/Lietuviu-kalbos-rasybos-tikrintuvai-bei-Hunspell-zodynai-gramatika',
            'section': 'Lietuvių kalba',
        },
        {
            'title': 'Lietuvių kalbos modelis',
            'text': 'Spacy Lietuvių kalbai',
            'link': 'https://spacy.io/models/lt',
            'section': 'Lietuvių kalba',
        },
    ],
    'Įsidarbink': [
        {
            'title': 'POV',
            'text': 'Programmers Of Vilnius naudoja Python tinklapių/appsų serverio daliai.',
            'link': 'https://pov.lt/jobs',
            'section': 'Tinklapių/appsų serverio dalis',
        },
        {
            'title': 'Corner Case Technologies',
            'text': 'Corner Case Technologies naudoja Python tinklapių/appsų serverio daliai.',
            'link': 'https://www.cornercasetech.com/career',
            'section': 'Tinklapių/appsų serverio dalis',
        },
        {
            'title': 'Shift4',
            'text': 'Shift4 Payments Lithuania naudoja Python tinklapių/appsų serverio daliai.',
            'link': 'https://www.shift4.com/careers#job-search-section',
            'section': 'Tinklapių/appsų serverio dalis',
        },
    ],
    'Apie': [],
}
