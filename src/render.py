"""Render data into Jinja2 template to generate public/index.html."""

from pathlib import Path

from jinja2 import Environment, FileSystemLoader
from unidecode import unidecode

from src.data import content_items


def slugify(name: str) -> str:
    """Create slug of a name by replacing characters to lower case ASCII and dashes."""
    return unidecode(name).lower().replace(' ', '-')


def render() -> None:
    """Render data into index.html template, write and return the content."""
    env = Environment(loader=FileSystemLoader('src/templates'))  # noqa: S701

    categories, items = {}, {}
    for i, (category, elements) in enumerate(content_items.items()):
        key = '/' if i == 0 else f'/{slugify(category)}.htm'
        categories[key] = category
        items[key] = elements

    context = {
        'categories': categories,
        'items': items,
    }

    # TODO: use menu items as top nesting and sections as a second level nesting.

    for i, category in enumerate(content_items.keys()):
        context['active'] = '/' if i == 0 else f'/{slugify(category)}.htm'

        filename = 'index.html' if i == 0 else context['active']
        page_content = env.get_template('index.html').render(**context)
        with Path(f'public/{filename}').open('w') as f:
            f.write(page_content)

    print('Sėkmingai atnaujinti statiniai puslapiai')


if __name__ == '__main__':
    render()
